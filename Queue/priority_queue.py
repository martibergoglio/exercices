def empty_queue():
    """Devuelve una cola vacia"""

    return []


def enqueue(q, item):
    """Mete cosas en la cola q
    Cargar tuplas de igual prioridad en una misma sub lista"""

    priority = item[0]
    #busco si hay items de igual prioridad
    finded, count = search_priority(q, priority)
    if finded:
        q[count].append(item)
    else:
        #si no hay creo sub lista nueva y apendo item
        q.append([priority])
        q[count].append(item)
    return q


def dequeue(q):
    """Devuelve el prox elemento de la cola q.
    No se puede usar sobre Colas vacias o tira IndexError"""

    queue_sort(q)
    result = None
    index = len(q) - 1
    if len(q[index]) == 1:
        q.pop(index)
        index -= 1
    if not is_empty(q[index]):
        result = q[index][1]
        q[index].pop(1)
    if len(q) == 1 and len(q[0]) == 1:
        q.pop(0)
    return result


def is_empty(q):
    """Return True if the queue is empty"""

    if len(q) == 0:
        return True
    else:
        return False


def search_priority(q, priority):
    """True si existe una sub lista con la prioridad del item a insertar
    Count con el indice de la sub lista"""

    find = False
    count = 0
    if len(q) > 0:
        while len(q) > 0:
            if q[0][0] == priority:
                find = True
                break
            count += 1
            q = q[1:]
    return find, count


def queue_sort(q):
    """Ordena la cola"""

    aux = 0
    for i in range(len(q) - 1):
        for j in range(len(q) - 1):
            if(q[j][0] > q[j + 1][0]):
                aux = q[j]
                q[j] = q[j + 1]
                q[j + 1] = aux
    return q