class PriorityQueue:

    def __init__(self):
        self.queue = []
        self.priority = None
        self.finded = None
        self.count = None

    def enqueue(self, item):
        self.priority = item[0]
        self.finded, self.count = self.__search_priority(self.priority, self.queue)
        if self.finded:
            self.queue[self.count].append(item)
        else:
            self.queue.append([self.priority])
            self.queue[self.count].append(item)
        return self.queue

    def dequeue(self):
        self.queue_sort()
        index = len(self.queue) - 1
        if len(self.queue[index]) == 1:
            self.queue.pop(index)
            index -= 1
        if not self.queue[index] == []:
            result = self.queue[index][1]
            self.queue[index].pop(1)
        if len(self.queue) == 1 and len(self.queue[0]) == 1:
            self.queue.pop(0)
        return result

    def is_empty(self):
        if len(self.queue) == 0:
            return True
        else:
            return False

    def __search_priority(self, priority, q):
        self.find = False
        self.count = 0
        if len(q) > 0:
            while len(q) > 0:
                if q[0][0] == priority:
                    self.find = True
                    break
                self.count += 1
                q = q[1:]
        return self.find, self.count

    def queue_sort(self):
        for i in range(len(self.queue) - 1):
            for j in range(len(self.queue) - 1):
                if self.queue[j][0] > self.queue[j + 1][0]:
                    aux = self.queue[j]
                    self.queue[j] = self.queue[j + 1]
                    self.queue[j + 1] = aux
        return self.queue