

def empty_queue():
    """Devuelve una cola vacia"""

    return []


def enqueue(q, item):
    """Mete cosas en la cola q"""

    q.append(item)
    return q


def is_empty(q):
    """Return True if the queue is empty"""
    if len(q) == 0:
        return True
    else:
        return False


def dequeue(q):
    """Devuelve el prox elemento de la cola q.
    No se puede usar sobre Colas vacias o tira IndexError"""
    #if(len(q) == 0):
        #q.remove(q[0])
    result = None
    if not is_empty(q):
        result = q[0]
        q.pop(0)
    return result