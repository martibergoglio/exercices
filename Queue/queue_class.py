class Queue:

    def __init__(self):
        self.queue = []
        self.result = None

    def enqueue(self, item):
        self.queue.append(item)

    def is_empty(self):
        if len(self.queue) == 0:
            return True
        else:
            return False

    def dequeue(self):
        if not self.is_empty():
            self.result = self.queue[0]
            self.queue.pop(0)
        return self.result