#from queue import*
from priority_queue import*



my_data = [
    (1, 'dato 1'),
    (1, 'dato  otro'),
    (2, 'datma sdf s o'),
    (4, 'datm sdf as o'),
    (2, 'dzxc atma s o'),
    (1, 'dat mass o'),
    (3, 'dzxc atma s o'),
    (8, 'dat mass o'),
]

#my_data = [
    #(1, [3,16]),
    #(1, [28,13]),
    #(2, [9,2]),
    #(4, [1,6]),
    #(2, [4,5]),
    #(1, [8,7]),
#]

q = empty_queue()

for item in my_data:
    enqueue(q, item)

print (q)

while not is_empty(q):
    i = dequeue(q)
    print("--------")
    print("priority: %i\ndata: %s" % (i[0], str(i[1])))

assert is_empty(q)
for i,x in enumerate([(1, 'dato 1'), (1, 'dato  otro'), (2, 'datma sdf s o'),
            (4, 'datm sdf as o'), (2, 'dzxc atma s o'), (1, 'dat mass o')]):
    assert my_data[i] == x

"""
---------
priority: 1
data: dato 1
---------
priority: 1
data: dato  otro
--------
...
"""

# Cola normal: Listo
#[(0,A), (0, B),...]
# (1, 'dato 1'),
# (1, 'dato  otro'),
# (2, 'datma sdf s o'),
# (4, 'datm sdf as o'),
# (2, 'dzxc atma s o'),
# (1, 'dat mass o'),


# Solucion 1:
#[ [(0,A), (0, B),...],  [(1, X), (1, Y),...]]

# (4, 'datm sdf as o'),
# (2, 'datma sdf s o'),
# (2, 'dzxc atma s o'),
# (1, 'dato 1'),
# (1, 'dato  otro'),
# (1, 'dat mass o'),



# Optimizacion:
#[  [itemA[1], itemB[1],...], [itemX, itemY,...], ... ]