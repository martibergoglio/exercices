#from queue_class import Queue
from priority_queue_class import PriorityQueue



my_data = [
    (1, 'dato 1'),
    (1, 'dato  otro'),
    (2, 'datma sdf s o'),
    (4, 'datm sdf as o'),
    (2, 'dzxc atma s o'),
    (1, 'dat mass o'),
    (3, 'dzxc atma s o'),
    (8, 'dat mass o'),
]

#my_queue = Queue()
my_queue = PriorityQueue()

for item in my_data:
    my_queue.enqueue(item)

while not my_queue.is_empty():
    i = my_queue.dequeue()
    print("--------")
    print("priority: %i\ndata: %s" % (i[0], str(i[1])))

assert my_queue.is_empty()
for i,x in enumerate([(1, 'dato 1'), (1, 'dato  otro'), (2, 'datma sdf s o'),
            (4, 'datm sdf as o'), (2, 'dzxc atma s o'), (1, 'dat mass o')]):
    assert my_data[i] == x