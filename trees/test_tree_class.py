from tree_class import TreeClass

tree = TreeClass()

tree.add(100)
tree.add(80)
tree.add(110)
tree.add(108)
tree.add(102)
tree.add(99)
tree.add(85)
tree.add(70)
tree.add(84)
tree.add(86)
tree.add(120)
tree.add(106)
tree.add(107)
tree.add(130)
tree.add(115)
tree.add(117)
tree.add(118)
tree.add(70)

print ('Tree Original: ')
print (tree)

#node_list = [100, 80, 110, 108, 99, 85, 70, 84, 86, 120, 130, 115, 117, 118, 77, 75, 73]

node_list = [100, 80, 110, 108, 102, 99, 70, 85, 86, 120, 106, 107, 130, 115, 117, 118, 84]


for node in node_list:
    print ('****************************************')
    print ('nodo a eliminar: ', node)
    tree.remove(node)
    print (tree)

# node = 100

# tree.remove(node)

print ('****************************************')
print ('Resultado final del arbol:')

print (tree)