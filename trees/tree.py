# -*- coding: utf-8 -*-

# Example Binary Search Tree implementation


def empty_node(parent=None):
    """Return empty tree"""

    return [None, None, None, parent]


def tree_add(tree, item, parent):
    """dado un arbol, un item y un padre=None,
    agrega nodo en el arbol en donde corresponda
    izq -> menores | derecha -> mayores"""

    # todos los nodos e hijos de la izquierda son menores
    if is_empty(tree):
        update_node(tree, empty_node(parent=tree),
            item, empty_node(parent=tree), parent)
    else:
        if item < node_value(tree):
            tree_add(left_branch(tree), item, tree)
            return
        elif item > node_value(tree):
            tree_add(right_branch(tree), item, tree)
            return
        else:
            return None


def tree_search(tree, item):
    """Return Node or None"""

    if is_empty(tree):
        return None
    else:
        if item == node_value(tree):
            return tree
        else:
            if item < node_value(tree):
                return tree_search(left_branch(tree), item)
            else:
                return tree_search(right_branch(tree), item)


def tree_remove(tree, item):
    """Return tree whithout node or None"""

    node = tree_search(tree, item)
    parent = get_parent(node)

    # Si el arbol no tiene ningun nodo
    if not node:
        return None
        ### Se podría hacer que esta función devuelva algo distinto si borró un
        ### nodo que cuando no lo hizo. Pensá un caso de uso donde eso pueda ser
        ### útil...

    # Si el arbol tiene 1 solo nodo
    elif not parent and is_leaf(node):
        update_node(node, None, None, None, None)
    else:
        # Si es una hoja
        if is_leaf(node):
            if node_value(node) > node_value(parent):
                parent[2] = empty_node()
            else:
                parent[0] = empty_node()
        # Si tiene 1 hijo
        elif one_children(node):
            # Si es hijo derecho
            if node_value(left_branch(node)) is None:
                right_node = right_branch(node)
                update_node(parent, left_branch(parent), node_value(parent),
                            right_node, get_parent(parent))
                update_node(right_node, left_branch(right_node),
                            node_value(right_node), right_branch(right_node),
                            parent)
            # Si es hijo izquierdo
            else:
                left_node = left_branch(node)
                update_node(parent, left_branch(parent), node_value(parent),
                            left_node, get_parent(parent))
                update_node(left_node, left_branch(left_node),
                            node_value(left_node), right_branch(left_node),
                            parent)
        # Si tiene 2 hijos
        else:
            # Si el nodo es hijo derecho
            if node_value(node) > node_value(parent):
                left_node = left_branch(node)
                # Nodo que va a susplantar al nodo a remover
                new_node = logic_search(left_node, True)
                update_node(parent, left_branch(parent), node_value(parent),
                    new_node, get_parent(parent))
                # Referencia al padre de left_node
                four_nodes = node_value(get_parent(left_node))
                # Si new_node tiene hijo izquierdo
                if left_branch(new_node) is not None:
                    # Referencia a hijo izquierdo de new_node
                    left_children = left_branch(new_node)
                    # Referencia a padre de new_node
                    new_node_parent = get_parent(new_node)
                    update_node(left_children, left_branch(left_children),
                        node_value(left_children), right_branch(left_children),
                            get_parent(new_node))
                    update_node(new_node, left_branch(node),
                        node_value(new_node), right_branch(node), parent)
                    # Si el padre de left_node NO es node
                    if node_value(node) == four_nodes:
                        update_node(new_node_parent,
                            left_branch(new_node_parent),
                                node_value(new_node_parent),
                                    left_children, new_node)
                    else:
                        update_node(new_node_parent,
                            left_branch(new_node_parent),
                                node_value(new_node_parent), left_children,
                                    get_parent(new_node_parent))
                # Si es una hoja
                else:
                    update_node(new_node, left_branch(node),
                        node_value(new_node), right_branch(node), parent)
            # Si el nodo es hijo izquierdo
            else:
                right_node = right_branch(node)
                # Nodo que susplanta al nodo a remover
                new_node = logic_search(right_node, False)
                update_node(parent, new_node, node_value(parent),
                    right_branch(parent), get_parent(parent))
                # Padre de right_node
                four_nodes = node_value(get_parent(right_node))
                # Si new_node tiene hijo derecho
                if right_branch(new_node) is not None:
                    right_children = right_branch(new_node)
                    new_node_parent = get_parent(new_node)
                    update_node(right_children, left_branch(right_children),
                        node_value(right_children),
                            right_branch(right_children),
                                get_parent(new_node))
                    update_node(new_node, left_branch(node),
                        node_value(new_node), right_branch(node), parent)
                    # Si el padre de right_node NO es node
                    if node_value(node) == four_nodes:
                        update_node(new_node_parent, right_children,
                            node_value(new_node_parent),
                                right_branch(new_node_parent), new_node)
                    else:
                        update_node(new_node_parent, right_children,
                            node_value(new_node_parent),
                                right_branch(new_node_parent),
                                    get_parent(new_node_parent))
                # Si es una hoja
                else:
                    update_node(new_node, left_branch(node),
                        node_value(new_node), right_branch(node),
                            parent)


###
# Auxiliar functions: Accessors
###


def is_empty(tree):
    """True if is empty"""

    return node_value(tree) is None


def node_value(tree):
    """Return the value of a given tree."""

    return tree[1]


def left_branch(tree):
    """Returns the Left sub-tree of the given tree."""

    return tree[0]


def right_branch(tree):
    """Return de right sub-tree of the given tree"""

    return tree[2]


def get_parent(tree):
    """Return the parent of a given tree"""

    return tree[3]


###
# My Accesors
###


def first_element(tree):
    """True if tree don´t have any elements"""

    for node in tree:
        if node is not None:
            return False
    return True


def children(node, direction):
    """True si un nodo NO tiene un hijo en uno de sus branch´s
    direction False -> izq | True -> der"""

    if direction:
        if node_value(right_branch(node)) is None:
            return True
    else:
        if node_value(left_branch(node)) is None:
            return True
    return False


def logic_search(node, direction):
    """Busca el nodo adecuado de acuerdo a la logica del arbol
    children -> si hay none devuelve el nodo"""

    if is_leaf(node) or children(node, direction):
        return node
    else:
        if direction:
            return logic_search(right_branch(node), direction)
        else:
            return logic_search(left_branch(node), direction)


def is_leaf(tree):
    """True if the node is the last of the tree"""

    if node_value(left_branch(tree)) is None and node_value(right_branch(tree)) is None:
        return True
    else:
        return False


def one_children(tree):
    """"True for 1 son"""

    if node_value(left_branch(tree)) is None or node_value(right_branch(tree)) is None:
            return True
    else:
        return False


###
# Auxiliar functions: Modifiers or setters
###


def update_node(node, left_branch, value, right_branch, father):
    """Create a node"""

    node[0] = left_branch
    node[1] = value
    node[2] = right_branch
    node[3] = father


###
# Testing functions
###


def node_searcher(tree, node):
    my_node = tree_search(tree, node)
    if my_node:
        print (("Encontrado: " + str(node_value(my_node))))
        print (("Padre: " + str(get_parent(my_node)[1])))
        print (("Left Children: " + str(left_branch(my_node)[1])))
        print (("Right Children: " + str(right_branch(my_node)[1])))
    else:
        print (my_node)


def testing_remove(tree, node):
    if tree_search(tree, node):
        print ("Lo encontre")
    else:
        print (('Eliminando nodo...Buscando el: ' + str(node) + '...No lo encontre'))
