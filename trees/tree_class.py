class TreeClass:

    def __init__(self, parent=None):
        self.node = None
        self.parent = parent
        self.leftBranch = None
        self.rightBranch = None

    def __str__(self):
        return ('[' + str(self.leftBranch) + " <-" + str(self.node) + "-> " + str(self.rightBranch) + ']')

    def add(self, value, parent=None):
        """Add one node to the tree"""

        if self.is_empty():
            self.node = value
            self.leftBranch = TreeClass()
            self.rightBranch = TreeClass()
            self.parent = parent
        else:
            if value < self.node:
                self.leftBranch.add(value, self)
                return
            elif value > self.node:
                self.rightBranch.add(value, self)
                return
            else:
                return None

    def search(self, value):
        """Return None if cant find value"""

        if self.is_empty():
            return self
        else:
            if self.node == value:
                return self
            else:
                if  value < self.node:
                    return self.leftBranch.search(value)
                else:
                    return self.rightBranch.search(value)

    def remove(self, value):
        """Remove the value from the tree"""

        node = self.search(value)

        if node.node is None:
            return None
        elif node.__is_leaf():
                node.node = None
                node.leftBranch = None
                node.rightBranch = None
        elif node.__one_branch():
            if node.leftBranch.node is not None:
                node.left_replace()
            else:
                node.right_replace()
        else:
            node.__logic_replace()


    ###
    #Auxiliar Functions
    ###

    def is_empty(self):
        return self.node is None

    def __is_leaf(self):
        """True or False"""

        if self.leftBranch.node is None and self.rightBranch.node is None:
            return True
        else:
            return False

    def __not_branch_in_direction(self, direction):
        """Auxiliary method of __logic_search() True if node doesnt have branch on specified direction and False
        if it does // True -> rightBranch | False -> leftBranch"""

        if direction:
            if self.rightBranch.node is None:
                return True
        else:
            if self.leftBranch.node is None:
                return True
        return False

    def __one_branch(self):
        """True or false"""

        if self.leftBranch.node is None or self.rightBranch.node is None:
            return True
        else:
            return False

    def __logic_search(self, direction):
        """Returns the new node to replace the elected
        True -> for left branch replace // False -> for right branch replace"""

        if self.__is_leaf() or self.__not_branch_in_direction(direction):
            return self
        else:
            if direction:
                return self.rightBranch.__logic_search(direction)
            else:
                return self.leftBranch.__logic_search(direction)

    def left_replace(self):
        """Replace elected node selecting a node from the left branch"""

        new_node = self.leftBranch.__logic_search(True)
        self.node = new_node.node
        if new_node.leftBranch.node is not None:
            if new_node.node == self.leftBranch.node:
                self.leftBranch = new_node.leftBranch
                new_node.leftBranch.parent = self
            else:
                new_node.parent.rightBranch = new_node.leftBranch
                new_node.leftBranch.parent = new_node.parent
        else:
            if self.leftBranch.node == new_node.node:
                self.leftBranch = TreeClass()
            else:
                new_node.parent.rightBranch = TreeClass()

    def right_replace(self):
        """Replace elected node selecting a node from the right branch"""

        new_node = self.rightBranch.__logic_search(False)
        self.node = new_node.node
        if new_node.rightBranch.node is not None:
            if new_node.node == self.rightBranch.node:
                self.rightBranch = new_node.rightBranch
                new_node.rightBranch.parent = self
            else:
                new_node.parent.leftBranch = new_node.rightBranch
                new_node.rightBranch.parent = new_node.parent
        else:
            if self.rightBranch.node == new_node.node:
                self.rightBranch = TreeClass()
            else:
                new_node.parent.leftBranch = TreeClass()

    def __logic_replace(self):
        """Replace elected node selecting a node from right branch or left branch
        depending of the nearest node value"""

        left_node = self.leftBranch.__logic_search(True)
        right_node = self.rightBranch.__logic_search(False)
        left_difference = self.node - left_node.node
        right_difference = right_node.node - self.node
        if left_difference < right_difference:
            self.left_replace()
        else:
            self.right_replace()