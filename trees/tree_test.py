from tree import *


t = empty_node()

tree_add(t, 10, None)
tree_add(t, 20, None)
tree_add(t, 30, None)
tree_add(t, 40, None)
tree_add(t, 25, None)
tree_add(t, 15, None)
tree_add(t, 13, None)
tree_add(t, 18, None)
tree_add(t, 2, None)
tree_add(t, -10, None)
tree_add(t, 9, None)
tree_add(t, 6, None)
tree_add(t, -5, None)
tree_add(t, 50, None)
tree_add(t, 45, None)
tree_add(t, 27, None)
tree_add(t, 24, None)
tree_add(t, 26, None)
tree_add(t, 7, None)

node_to_remove = 30


if tree_search(t, node_to_remove):
    tree_remove(t, node_to_remove)
    print ('ok')


print ("**************************************")

testing_remove(t, node_to_remove)

print ("**************************************")

node_searcher(t, 25)

print ("**************************************")

node_searcher(t, 27)

print ("**************************************")

node_searcher(t, 26)